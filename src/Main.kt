fun main() {
    val v = ""
    println(v)
    print("Hello world")
    if (1 != 1) {
        println("Never reachable")
    }
    if (1 == 1) {
        println("Always reachable")
    }
    var unused: Int
}

fun f() {}
